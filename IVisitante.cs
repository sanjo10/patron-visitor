﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VISITOR
{
    interface IVisitante
    {

        //Método Visitar para los 2 tipos de elementos

        void Visitar(Elemento pElemento);
        void Visitar(ElementoContiene pElemento);
    }
}

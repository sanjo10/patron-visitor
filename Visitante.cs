﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VISITOR
{
    class Visitante : IVisitante
    {
        private int conteo;
        private int tipos;
        private double total;

        //Setters y Getters para las variables privadas
        public void setConteo(int num)
        {
            this.conteo = num;
        }
        public int getConteo()
        {
            return this.conteo;
        }

        public void setTipos(int tipo)
        {
            this.tipos = tipo;
        }
        public int getTipos()
        {
            return this.tipos;
        }

        public void setTotal(double total)
        {
            this.total = total;
        }
        public double getTotal()
        {
            return this.total;
        }


        //Contaremos todos los elementos declarados y verificamos si están nulos o no
        public void ContarElementos(Elemento elemento)
        {
            //Aceptamos al visitante
            elemento.Aceptar(this);

            //Verificamos Si tiene hijos
            if(elemento.hijo != null)
            {
                ContarElementos(elemento.hijo);
            }

            //Verificamos si tenemos siguiente
            if(elemento.siguiente != null)
            {
                ContarElementos(elemento.siguiente);
            }
        }

        public void Visitar(Elemento elemento)
        {
            conteo++;
            total += elemento.getPrecio();
        }

        public void Visitar(ElementoContiene elemento)
        {
            this.tipos++;
        }
    }
}

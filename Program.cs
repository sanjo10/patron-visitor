﻿using System;

namespace VISITOR
{
    class Program
    {
    
        //Estructuramos un Objeto de Elementos
        static Elemento Elementos =
            new Elemento(150000, "Casa",
            new Elemento(45000, "Auto",
            new ElementoContiene(
                new Elemento(300, "Aire Acondicionado",
                new Elemento(400, "Ruedas",
                new ElementoContiene(
                new Elemento(200, "Rueda Delantera", null),
                new Elemento(200, "Rueda Trasera",null)))),
            new Elemento(400, "Cocina",
            new Elemento(1600, "Televisor",
            new Elemento(70, "Microondas", null))))));
        static void Main(string[] args)
        {
            double totalPrecio = 0;
            int totalElementos = 0;
            int totalTipos = 0;

            //Visitamos los Elementos
            Visitante visitante = new Visitante();
            visitante.ContarElementos(Elementos);

            totalPrecio = visitante.getTotal();

            totalTipos = visitante.getTipos();
            
            totalElementos = visitante.getConteo();

            Console.WriteLine("[------ Detalles ------]");
            Console.WriteLine("Total de Elemento: " + totalElementos);
            Console.WriteLine("Valor Total: " + totalPrecio);
            Console.WriteLine("Tipos de elementos: " + totalTipos);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VISITOR
{
    interface IElemento
    {
        void Aceptar(IVisitante visitante);
    }
}

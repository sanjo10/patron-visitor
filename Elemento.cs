﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VISITOR
{
    class Elemento:IElemento
    {
        public Elemento siguiente;
        public Elemento hijo;
        private double precio;
        private string nombre;


        //Setters y Getters para las variables privadas
        public void setPrecio(double valor)
        {
            this.precio = valor;
        }
        public double getPrecio()
        {
            return this.precio;
        }

        public void setNombre(string nombre)
        {
            this.nombre = nombre;
        }
        public string getNombre()
        {
            return nombre;
        }

        public Elemento ()
        {
            Console.WriteLine("Elemento Credo Exitósamente");
        }

        public Elemento(double valor, string nombre, Elemento pSiguiente)
        {
            siguiente = pSiguiente;
            setPrecio(valor);
            setNombre(nombre);

            Console.WriteLine("Elemento creado, los datos son los siguientes: ");
            Console.WriteLine("Nombre: " + nombre);
            Console.WriteLine("Precio: " + precio);
            
        }

        //Método para aceptar visitante
        public virtual void Aceptar(IVisitante pVisitante)
        {
            //Mandamos este elemento para que visitante procese
            pVisitante.Visitar(this);
        }
    }
}

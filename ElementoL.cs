﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VISITOR
{
    class ElementoContiene:Elemento
    {

        //Constructor de ElementoContiene
        public ElementoContiene(Elemento pHijo, Elemento pSiguiente)
        {
            siguiente = pSiguiente;
            hijo = pHijo;
        }

        //Método para aceptar visitante
        public override void Aceptar(IVisitante pVisitante)
        {
            //Mandamos este elemento para que visitante procese
            pVisitante.Visitar(this);
        }
    }
}
